import React from "react";
import { StyleSheet, Text, TextInput, View } from "react-native";
import { COLORS } from "../../../utils";

const FormInput = ({
  labelText = "",
  placeholderText = "",
  onChangeText = null,
  value = null,
  secureTextEntry = false,
  ...more
}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.label}>{labelText}</Text>
      <TextInput
        style={styles.input}
        placeholder={placeholderText}
        onChangeText={onChangeText}
        value={value}
        secureTextEntry={secureTextEntry}
        {...more}
      />
    </View>
  );
};

export default FormInput;

const styles = StyleSheet.create({
  container: { width: "100%", marginBottom: 20 },
  label: { marginBottom: 10 },
  input: {
    padding: 10,
    borderColor: COLORS.black + "20",
    borderWidth: 1,
    width: "100%",
  },
});
