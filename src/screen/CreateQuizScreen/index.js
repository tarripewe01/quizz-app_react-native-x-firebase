import React, { useState } from "react";
import {
  SafeAreaView,
  StyleSheet,
  Text,
  ToastAndroid,
  View,
} from "react-native";
import { Button, FormInput } from "../../components";
import { createQuiz } from "../../firebase";
import { COLORS } from "../../utils";

const CreateQuizScreen = ({ navigation }) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  const handleQuizSave = async () => {
    const currentQuizId = Math.floor(100000 + Math.random() * 9000).toString();
    // Save to Firestore
    await createQuiz(currentQuizId, title, description);

    // Navigate
    navigation.navigate("AddQuestionScreen", {
      currentQuizId: currentQuizId,
      currentQuizTitle: title,
    });

    // Reset Form
    setTitle("");
    setDescription("");
    ToastAndroid.show("Quiz Saved", ToastAndroid.SHORT);
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.white }}>
      <View style={{ padding: 20 }}>
        <Text
          style={{
            fontSize: 20,
            textAlign: "center",
            marginVertical: 20,
            fontWeight: "bold",
            color: COLORS.black,
          }}
        >
          Create Quiz
        </Text>

        <FormInput
          labelText="Title"
          placeholderText="Enter your title"
          value={title}
          onChangeText={(val) => setTitle(val)}
        />

        <FormInput
          labelText="Description"
          placeholderText="Enter your description"
          value={description}
          onChangeText={(val) => setDescription(val)}
        />

        <Button labelText="Save Quiz" handleOnPress={handleQuizSave} />
      </View>
    </SafeAreaView>
  );
};

export default CreateQuizScreen;

const styles = StyleSheet.create({});
