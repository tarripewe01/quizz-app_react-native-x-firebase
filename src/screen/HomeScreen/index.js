import React from "react";
import { View, Text } from "react-native";
import { signOut } from "../../firebase/AuthFirebase";
import { Button } from "../../components";
import { SafeAreaView } from "react-native-safe-area-context";
import { COLORS } from "../../utils";

const HomeScreen = ({ navigation }) => {
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.white }}>
      <View style={{ padding: 20 }}>
        <Text>HomeScreen</Text>
        <Text onPress={signOut}>Logout</Text>

        <Button
          labelText="Create Quiz"
          handleOnPress={() => navigation.navigate("CreateQuizScreen")}
        />
      </View>
    </SafeAreaView>
  );
};

export default HomeScreen;
