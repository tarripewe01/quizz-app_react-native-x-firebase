import React, { useState } from "react";
import { View, Text, SafeAreaView, StyleSheet } from "react-native";
import { FormInput, Button } from "../../components";
import { signIn } from "../../firebase/AuthFirebase";
import { COLORS } from "../../utils";

const SignInScreen = ({ navigation }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleOnSubmit = () => {
    if (email != "" && password != "") {
      signIn(email, password);
    }
  };
  return (
    <SafeAreaView style={styles.page}>
      <View style={styles.container}>
        <Text style={styles.title}>Sign In</Text>

        <FormInput
          labelText="Email"
          placeholderText="Enter your email"
          value={email}
          onChangeText={(value) => setEmail(value)}
          keyboardType={"email-address"}
        />

        <FormInput
          labelText="Password"
          placeholderText="Enter your password"
          value={password}
          onChangeText={(value) => setPassword(value)}
          secureTextEntry={true}
        />

        <Button
          labelText="Submit"
          handleOnPress={handleOnSubmit}
          style={styles.button}
        />

        <View style={styles.containCreate}>
          <Text>Don't have an account?</Text>
          <Text
            onPress={() => navigation.navigate("SignUpScreen")}
            style={styles.action}
          >
            Create account
          </Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default SignInScreen;

const styles = StyleSheet.create({
  page: {
    backgroundColor: COLORS.white,
    flex: 1,
  },
  container: {
    paddingHorizontal: 20,
    paddingVertical: 32,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "flex-start",
  },
  title: {
    fontSize: 26,
    fontWeight: "bold",
    color: COLORS.black,
    marginBottom: 50,
  },
  button: { width: "100%" },
  containCreate: { flexDirection: "row", alignItems: "center", marginTop: 20 },
  action: { marginLeft: 5, color: COLORS.primary },
});
