import React, { useState } from "react";
import { View, Text, StyleSheet, SafeAreaView, Alert } from "react-native";
import { Button, FormInput } from "../../components";
import { signUp } from "../../firebase/AuthFirebase";
import { COLORS } from "../../utils";

const SignUpScreen = ({ navigation }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const handleOnSubmit = () => {
    if (email != "" && password != "" && confirmPassword != "") {
      if (password == confirmPassword) {
        signUp(email, password);
      } else {
        Alert.alert("password did not match");
      }
    }
  };
  return (
    <SafeAreaView style={styles.page}>
      <View style={styles.container}>
        <Text style={styles.title}>Sign Up</Text>

        <FormInput
          labelText="Email"
          placeholderText="Enter your email"
          value={email}
          onChangeText={(value) => setEmail(value)}
          keyboardType={"email-address"}
        />

        <FormInput
          labelText="Password"
          placeholderText="Enter your password"
          value={password}
          onChangeText={(value) => setPassword(value)}
          secureTextEntry={true}
        />

        <FormInput
          labelText="Confirm Password"
          placeholderText="Enter your confirm password"
          value={confirmPassword}
          onChangeText={(value) => setConfirmPassword(value)}
          secureTextEntry={true}
        />

        <Button
          labelText="Sign Up"
          handleOnPress={handleOnSubmit}
          style={styles.button}
        />

        <View style={styles.containCreate}>
          <Text>Already have an account?</Text>
          <Text
            onPress={() => navigation.goBack("SignInScreen")}
            style={styles.action}
          >
            Sign In
          </Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default SignUpScreen;

const styles = StyleSheet.create({
  page: {
    backgroundColor: COLORS.white,
    flex: 1,
  },
  container: {
    paddingHorizontal: 20,
    paddingVertical: 32,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "flex-start",
  },
  title: {
    fontSize: 26,
    fontWeight: "bold",
    color: COLORS.black,
    marginBottom: 50,
  },
  button: { width: "100%" },
  containCreate: { flexDirection: "row", alignItems: "center", marginTop: 20 },
  action: { marginLeft: 5, color: COLORS.primary },
});
