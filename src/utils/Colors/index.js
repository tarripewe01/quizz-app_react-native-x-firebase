export const COLORS = {
  primary: "#4630EB",
  secondary: "#000020",

  success: "#00c851",
  error: "#FF4444",

  black: "#171717",
  white: "#FFFFFF",
  background: "#F4F4F4",
  border: "#F5F5F7",
};
